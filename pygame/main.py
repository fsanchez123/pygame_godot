import pygame

#all global variables
FPS=60
WINDOW_SIZE_X=800
WINDOW_SIZE_Y=600
PLAYER_SPD=0.2
BULLET_SPD=0.4


#function for moving player in the screen
def move_player(player, delta):
    moved=False
    idle=False
    spd=int(PLAYER_SPD*delta)#adjust the player speed to delta time so player doesn't go faster when the fps are increased (or slowed if are decreased)
    keys = pygame.key.get_pressed()
    if keys[pygame.K_RIGHT]:
        player['x']=min(player['x']+spd, WINDOW_SIZE_X-50)
        player['sprites']=player['right']
        moved=True
    elif keys[pygame.K_UP]:
        player['y']=max(player['y']-spd, 0)
        player['sprites']=player['up']
        moved=True
    elif keys[pygame.K_DOWN]:
        player['y']=min(player['y']+spd, WINDOW_SIZE_Y-50)
        player['sprites']=player['down']
        moved=True
    else:
        player['x']=max(player['x']-(spd/3), 0)
        player['sprites']=player['idle']
        idle=True

    if moved:
        if player['sprite_index']==2:
            player['sprite_index']=0
        player['animation_time']+=delta
        if player['animation_time']>400:
            player['animation_time']-=400
            player['sprite_index']=(player['sprite_index']+1)%2
    if idle:
        player['animation_time']+=delta
        if player['animation_time']>200:
            player['animation_time']-=200
            player['sprite_index']=(player['sprite_index']+1)%3

#function to create the player with all his variables
def create_player():
    player={
        'right': [
            pygame.image.load('sprites/right1.png'),
            pygame.image.load('sprites/right2.png')
        ],
        'idle': [
            pygame.image.load('sprites/idle1.png'),
            pygame.image.load('sprites/idle2.png'),
            pygame.image.load('sprites/idle3.png')
        ],
        'up': [
            pygame.image.load('sprites/up1.png'),
            pygame.image.load('sprites/up2.png')
        ],
        'down': [
            pygame.image.load('sprites/down1.png'),
            pygame.image.load('sprites/down2.png')
        ],
        'sprite_index': 0,
        'animation_time': 0,
        'x':400-29,
        'y':300-30
    }
    player['sprites']=player['idle']
    return player

#function to draw the player in the screen
def draw_player(screen, player):
    sprite=player['sprites'][player['sprite_index']]
    square=sprite.get_rect().move(player['x'], player['y'])
    screen.blit(sprite, square)

#main function where the screen is setted adn the game loop happens
def main():
    pygame.init()
    screen = pygame.display.set_mode([WINDOW_SIZE_X, WINDOW_SIZE_Y])
    player=create_player()

    pygame.display.set_caption('SpaceShips')

    clock = pygame.time.Clock()
    going=True
    while going:
        delta=clock.tick(FPS)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going=False
        screen.fill((128,128,128))
        move_player(player, delta)
        draw_player(screen, player)
        pygame.display.flip()
    pygame.quit()

main()
